//
//  CreationManager.swift
//  MyFirstGame
//
//  Created by Christopher Whitehead on 9/06/2014.
//  Copyright (c) 2014 Christopher Whitehead. All rights reserved.
//  cd Projects/XCode/MyFirstGame
//  commit (opens VI editor)
//  git push ServerRepoGameSK master
//  http://68.64.172.195/Bonobo.Git.Server/Repository/Index
// chris
// 2ndSumm3r

import Foundation
import SpriteKit



struct CreateManager {
    
    static func createBackground(parent : SKScene)->(background : SKSpriteNode, parallax : ParallaxSprite[]){
        let background = SKSpriteNode(imageNamed: "BGSky");
        background.position = CGPointMake(CGRectGetMidX(parent.frame), CGRectGetMidY(parent.frame));
        parent.addChild(background);
        
        let layer1 = ParallaxSprite(name: "BGCloud", speed: 1, frame: parent.frame);
        let layer2 = ParallaxSprite(name: "BGCity", speed: 2, frame: parent.frame);
        let layer3 = ParallaxSprite(name: "BGGrass", speed: 4, frame: parent.frame);
        
        parent.addChild(layer1);
        parent.addChild(layer2);
        parent.addChild(layer3);
        
        return (background, [layer1, layer2, layer3]);
    }
    
    static func createCharacter(parent : SKScene)->SKSpriteNode{
        let atlas : SKTextureAtlas = SKTextureAtlas(named: "ninja")
        let f1 : SKTexture = atlas.textureNamed("1.png")
        let f2 : SKTexture = atlas.textureNamed("2.png")
        let f3 : SKTexture = atlas.textureNamed("3.png")
        let f4 : SKTexture = atlas.textureNamed("4.png")
        let f5 : SKTexture = atlas.textureNamed("5.png")
        let f6 : SKTexture = atlas.textureNamed("6.png")
        let f7 : SKTexture = atlas.textureNamed("7.png")
        let f8 : SKTexture = atlas.textureNamed("8.png")
 
        
        let characterFrames : Array = [f1,f2,f3,f4,f5,f6,f7,f8];
        
        let player : SKSpriteNode = SKSpriteNode(imageNamed: "1.png")
        player.position =  CGPointMake(CGRectGetMidX(parent.frame) - 50, CGRectGetMidY(parent.frame) + 50)
        
        parent.addChild(player)
        
        let action : SKAction = SKAction.repeatActionForever(SKAction.animateWithTextures(characterFrames, timePerFrame: 0.1))
        player.runAction(action)
        
        player.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: player.size.width - 25, height: player.size.height - 25))
        player.physicsBody.categoryBitMask = ContactCategory.player
        player.physicsBody.collisionBitMask = ContactCategory.player
        player.physicsBody.contactTestBitMask = ContactCategory.player
        
        return player;
    }
    
    static func createStar(parent : SKScene)->SKSpriteNode{
        let star : SKSpriteNode = SKSpriteNode(imageNamed: "Star");
        let rnd : CGFloat = CGFloat(arc4random() % 100);
        star.position.x = parent.frame.width + star.size.width;
        star.position.y = CGFloat(CGRectGetMidY(parent.frame) - 10 + rnd);
        
        
        star.physicsBody = SKPhysicsBody(rectangleOfSize: star.size);
        star.physicsBody.categoryBitMask = ContactCategory.star;
        star.physicsBody.collisionBitMask = ContactCategory.star | ContactCategory.player;
        star.physicsBody.contactTestBitMask = ContactCategory.star | ContactCategory.player;
        
        parent.addChild(star);
        
        return star;
    }
}
