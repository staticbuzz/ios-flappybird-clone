//
//  GameScene.swift
//  MyFirstGame
//
//  Created by Christopher Whitehead on 5/06/2014.
//  Copyright (c) 2014 Christopher Whitehead. All rights reserved.
//

import SpriteKit
var score:Int = 0;
class GameScene: SKScene {
    var timeSinceFired: CFTimeInterval = 0.0
    var markTime: CFTimeInterval = 0.0
    var playerSprite = SKSpriteNode()
    var topPole = SKSpriteNode()
    var bottomPole = SKSpriteNode()
    let bg1 = SKSpriteNode(imageNamed:"BG")
    let bg2 = SKSpriteNode(imageNamed:"BG")
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        let myLabel = SKLabelNode(fontNamed:"Chalkduster")
        myLabel.name = "scoreLabel"
        myLabel.text = "Score: \(score)";
        myLabel.fontColor = SKColor(red: 0.9, green: 0.05, blue: 0.05, alpha: 1.0)
        myLabel.fontSize = 45;
        myLabel.position = CGPoint(x:CGRectGetWidth(self.frame) * 0.8, y:CGRectGetHeight(self.frame) - 40)
       
        
        // create 2 background sprites
        bg1.anchorPoint = CGPointZero;
        bg1.size = CGSize(width:CGRectGetWidth(self.frame), height:CGRectGetHeight(self.frame))
        bg1.position = CGPointMake(0, 0);
        self.addChild(bg1)
        bg2.anchorPoint = CGPointZero;
        bg2.size = CGSize(width:CGRectGetWidth(self.frame), height:CGRectGetHeight(self.frame))
        bg2.position = CGPointMake(bg1.size.width-1, 0);
        self.addChild(bg2)
        
        
        self.addChild(myLabel)
        self.addChild(setUpPlayer())
        //self.addChild(drawBarriers().0); self.addChild(drawBarriers().1); self.addChild(drawBarriers().2);
        
        self.physicsWorld.gravity = CGVectorMake(0,-4)
        
        var isJump : Bool = false;
        
        var background : SKSpriteNode!
        var parallax : ParallaxSprite[]!
        var player : SKSpriteNode!
        var stars : SKSpriteNode[] = [];

    }
    
    
    func setUpPoles() {
        topPole = setUpTopPole()
        bottomPole = setUpBottomPole()
        self.addChild(topPole)
        self.addChild(bottomPole)
    }
    
    
    override func mouseDown(theEvent: NSEvent) {
        /* Called when a mouse click occurs */
        var randY: Int
        var randX: Int
        let location = theEvent.locationInNode(self)
        randY = (Int)(arc4random() % 5 + 1)
        randX = (Int)(arc4random() % 5 + 1)
        let sprite = SKSpriteNode(imageNamed:"Sheep")
        sprite.position = CGPoint(x:location.x,y:location.y)
        sprite.setScale(0.1)
        //let action = SKAction.rotateByAngle(M_PI, duration:1)
        //sprite.runAction(SKAction.repeatActionForever(action))
        let action2 = SKAction.moveByX(CGFloat(randX * 5 * randY), y: 0, duration: CGFloat(1))
        let action3 = SKAction.moveByX(CGFloat(-randX * 5 * randX), y: 0, duration: CGFloat(1))
        let bounce = SKAction.sequence([action2, action3])
        sprite.runAction(SKAction.repeatActionForever(bounce))
        sprite.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: sprite.size.width - 20, height: sprite.size.height - 30))
        sprite.physicsBody.affectedByGravity = true
        sprite.physicsBody.mass = 2
        sprite.physicsBody.allowsRotation = false;
        self.addChild(sprite)
    }
    
    override func keyDown(theEvent: NSEvent) {
        switch (theEvent.keyCode) {
        case 0: //a
            playerSprite.runAction(SKAction.moveByX(-20, y: 0, duration: 1))
        case 2: //d
            playerSprite.runAction(SKAction.moveByX(20, y: 0, duration: 1))
        case 13: //w
            playerSprite.physicsBody.velocity = CGVector(0,400)

        default:
            println("\(theEvent.keyCode)")
        }
    }
    
    
    
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        timeSinceFired = currentTime - markTime
        bg1.position = CGPointMake(bg1.position.x-4, bg1.position.y);
        bg2.position = CGPointMake(bg2.position.x-4, bg2.position.y);
        
        if bg1.position.x < -bg1.size.width {
            bg1.position = CGPointMake(bg2.position.x + bg2.size.width, bg1.position.y)
        }
        
        if bg2.position.x < -bg2.size.width {
            bg2.position = CGPointMake(bg1.position.x + bg1.size.width, bg2.position.y)
        }
        
        if timeSinceFired > 5
        {
            setUpPoles()
            timeSinceFired = 0
            markTime = currentTime
            
        }
        
        topPole.physicsBody.velocity = CGVector(-100,0)
        bottomPole.physicsBody.velocity = CGVector(-100,0)
        
        if topPole.position.x < 0.5
        {
            topPole.removeFromParent()
            bottomPole.removeFromParent()
        }
            }
    
    func setUpPlayer()->SKSpriteNode{
        var randPos = arc4random() % 10
        let location = CGPoint(x:CGRectGetWidth(self.frame)*0.5, y:CGRectGetHeight(self.frame)*0.9)
        playerSprite = SKSpriteNode(imageNamed:"Fatlady")
        playerSprite.position = location
        playerSprite.setScale(0.23)
        playerSprite.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: playerSprite.size.width - 10, height: playerSprite.size.height - 10))
        playerSprite.physicsBody.affectedByGravity = true
        return playerSprite
    }
    
    func setUpTopPole()->SKSpriteNode{
        let randNum = arc4random() % 350 + 50
        let sprite = SKSpriteNode(color: SKColor(red: 255, green: 0, blue: 0, alpha: 1.0), size: CGSize(width: 50, height: CGFloat(randNum)))
        sprite.setScale(1)
        sprite.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: 50, height: CGFloat(randNum)))
        let location = CGPoint(x:CGRectGetWidth(self.frame), y:CGRectGetHeight(self.frame) - CGFloat(randNum / 2))
        sprite.position = location
        sprite.zPosition = 100
        sprite.physicsBody.affectedByGravity = false
        sprite.physicsBody.mass = 200000
        sprite.physicsBody.allowsRotation = false;
        sprite.name = "top"
        return sprite
    }
    
    func setUpBottomPole()->SKSpriteNode{
        let sprite = SKSpriteNode(color: SKColor(red: 0, green: 0, blue: 255, alpha: 1.0), size: CGSize(width: 50, height: 500 - topPole.size.height))
        sprite.setScale(1)
        sprite.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: 50, height: sprite.size.height))
        let location = CGPoint(x:CGRectGetWidth(self.frame), y: sprite.size.height / 2)
        sprite.position = location
        sprite.zPosition = 100
        sprite.physicsBody.affectedByGravity = false
        sprite.physicsBody.mass = 200000
        sprite.physicsBody.allowsRotation = false;
        sprite.name = "bottom"
        return sprite
    }


    
    
    
    /*func drawBarriers()->(SKSpriteNode,SKSpriteNode,SKSpriteNode){
        let barrierSprite = SKSpriteNode(color: SKColor(red: 255, green: 0, blue: 0, alpha: 1.0), size: CGSize(width: CGFloat(CGRectGetWidth(self.frame) - 40),height: 10))
        let location = CGPoint(x:CGRectGetMidX(self.frame), y:10)
        barrierSprite.zPosition = 10
        barrierSprite.position = location
        barrierSprite.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: barrierSprite.size.width, height: barrierSprite.size.height))
        barrierSprite.physicsBody.dynamic = false
        
        let leftBlockSprite = SKSpriteNode(color: SKColor(red: 0, green: 0, blue: 255, alpha: 1.0), size: CGSize(width: CGFloat(CGRectGetWidth(self.frame) / 2),height: 10))
        let locationLB = CGPoint(x:CGRectGetMidX(self.frame), y:200)
        leftBlockSprite.zPosition = 10
        leftBlockSprite.position = locationLB
        leftBlockSprite.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: leftBlockSprite.size.width, height: leftBlockSprite.size.height))
        leftBlockSprite.physicsBody.dynamic = true
        leftBlockSprite.physicsBody.mass = 200
        leftBlockSprite.physicsBody.affectedByGravity = false
        
        let rightBlockSprite = SKSpriteNode(color: SKColor(red: 0, green: 255, blue: 0, alpha: 1.0), size: CGSize(width: CGFloat(CGRectGetWidth(self.frame) / 2),height: 10))
        let locationRB = CGPoint(x:CGRectGetMidX(self.frame) + 240, y:350)
        rightBlockSprite.zPosition = 10
        rightBlockSprite.position = locationRB
        rightBlockSprite.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: rightBlockSprite.size.width, height: rightBlockSprite.size.height))
        rightBlockSprite.physicsBody.dynamic = false
        return (barrierSprite,leftBlockSprite, rightBlockSprite)
    }*/
}
