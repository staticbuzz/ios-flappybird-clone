//
//  GameScene.swift
//  MyFirstGame
//
//  Created by Christopher Whitehead on 5/06/2014.
//  Copyright (c) 2014 Christopher Whitehead. All rights reserved.
//  cd Projects/XCode/MyFirstGame
//  commit (opens VI editor)
//  git push ServerRepoGameSK master
//  http://68.64.172.195/Bonobo.Git.Server/Repository/Index
// chris
// 2ndSumm3r

import SpriteKit
import Cocoa
var score:Int = 0
var characterPosY = 0
var inMotion = false, characterCreated = false

struct ContactCategory {
    static let player    : UInt32 = 0x1 << 0
    static let star      : UInt32 = 0x1 << 1
    static let pole      : UInt32 = 0x1 << 2
    static let poleB      : UInt32 = 0x1 << 3
    static let poleMid      : UInt32 = 0x1 << 4
}
 

// remember to add SKPhysicsContactDelegate for collisions
class GameScene: SKScene, SKPhysicsContactDelegate {
    var timeSinceFired: CFTimeInterval = 0.0
    var markTime: CFTimeInterval = 0.0
    var playerSprite: SKSpriteNode!
    var topPole = SKSpriteNode()
    var bottomPole = SKSpriteNode()
    var midPole = SKSpriteNode()
    var background : SKSpriteNode!
    var parallax : ParallaxSprite[]!
    var player : SKSpriteNode!
    var stars : SKSpriteNode[] = []
    //create an empty array -- don't forget [] array initialiser
    var poles : SKSpriteNode[] = []
    var myLabel = SKLabelNode(fontNamed:"Chalkduster")
    
    override func didMoveToView(view: SKView) ->(){
        /* Setup your scene here */
        let ref = CreateManager.createBackground(self)
        background = ref.background
        parallax = ref.parallax
        gameWelcome()
        self.physicsWorld.gravity = CGVectorMake(0,-4)
        self.physicsWorld.contactDelegate = self
    }
    
    func setInMotion()->()
    {
        myLabel.text = "Score: \(score)";
        myLabel.fontColor = SKColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        myLabel.fontSize = 45
        myLabel.zPosition = 1001
        myLabel.position = CGPoint(x:CGRectGetWidth(self.frame) * 0.8, y:CGRectGetHeight(self.frame) - 40)
        playerSprite = CreateManager.createCharacter(self);
        characterCreated = true

    }
    
    func gameWelcome()->(){
        myLabel.name = "scoreLabel"
        myLabel.text = "Press any key to begin!";
        myLabel.fontColor = SKColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        myLabel.fontSize = 60
        myLabel.zPosition = 1001
        myLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        self.addChild(myLabel)
    }
    
    
    func didBeginContact(contact : SKPhysicsContact){
        var firstBody : SKPhysicsBody;
        var secondBody : SKPhysicsBody;
        
        if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
            firstBody = contact.bodyA;
            secondBody = contact.bodyB;
        } else {
            firstBody = contact.bodyB;
            secondBody = contact.bodyA;
        }
        
        if ((firstBody.categoryBitMask & ContactCategory.player) != 0) {
            if (secondBody.categoryBitMask == ContactCategory.poleMid)
            {
                score++;
            }
            else
            {
                 playerSprite.removeFromParent()
               // gameOver()
            }
            //self.runAction(sound);
        }
    }

    
    
    func setUpPoles() ->() {
        topPole = setUpTopPole()
        bottomPole = setUpBottomPole()
        midPole = setUpMidPole()
        self.addChild(topPole)
        self.addChild(bottomPole)
        self.addChild(midPole)
    }
    
    
    override func mouseDown(theEvent: NSEvent)->() {
        /* Called when a mouse click occurs */
        inMotion = true
        if !characterCreated
        {
            setInMotion()
        }
        playerSprite.physicsBody.velocity = CGVector(0,300)
    }
    
    
    override func keyDown(theEvent: NSEvent) {
        switch (theEvent.keyCode) {
        case 49: //spacebar
            inMotion = true
            if !characterCreated
            {
                setInMotion()
            }
            playerSprite.physicsBody.velocity = CGVector(0,300)
        default:
            println("\(theEvent.keyCode)")
        }
    }
    
    
    
    
    override func update(currentTime: CFTimeInterval) {
        if inMotion
        {
        /* Called before each frame is rendered */
        timeSinceFired = currentTime - markTime

        if timeSinceFired > 1.5
        {
            setUpPoles()
            timeSinceFired = 0
            markTime = currentTime
            
        }
        
        if topPole.position.x < 0.5
        {
           // topPole.removeFromParent()
           // bottomPole.removeFromParent()
           // midPole.removeFromParent()
            poles.removeAtIndex(0)
            poles.removeAtIndex(0)
            poles.removeAtIndex(0)
        }
        
        for p : ParallaxSprite in parallax {
            p.update();
        }
        
        //for every pole in array of poles, move slowly off the screen
        for p in poles {
            p.runAction(SKAction.moveByX(-4, y: 0, duration: 1))

        }
        }

        if characterCreated
        {
            characterPosY = (Int)(playerSprite.position.y)
            myLabel.text = "Score: \(score)"
        }
        
    }
    
    func setUpTopPole()->SKSpriteNode{
        let randNum = arc4random() % 350 + 50
        let sprite = SKSpriteNode(color: SKColor(red: 255, green: 0, blue: 0, alpha: 1.0), size: CGSize(width: 60, height: CGFloat(randNum)))
        sprite.setScale(1)
        sprite.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: 60, height: CGFloat(randNum)))
        let location = CGPoint(x:CGRectGetWidth(self.frame) + 60, y:CGRectGetHeight(self.frame) - CGFloat(randNum / 2))
        sprite.position = location
        sprite.zPosition = 100
        sprite.physicsBody.affectedByGravity = false
        sprite.physicsBody.mass = 200000
        sprite.physicsBody.allowsRotation = false;
        sprite.name = "top"
        sprite.physicsBody.categoryBitMask = ContactCategory.pole
        sprite.physicsBody.collisionBitMask = ContactCategory.pole | ContactCategory.player
        sprite.physicsBody.contactTestBitMask = ContactCategory.pole | ContactCategory.player
        poles.append(sprite)
        return sprite
    }
    
    func setUpBottomPole()->SKSpriteNode{
        let sprite = SKSpriteNode(color: SKColor(red: 0, green: 0, blue: 255, alpha: 1.0), size: CGSize(width: 60, height: 500 - topPole.size.height))
        sprite.setScale(1)
        sprite.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: 60, height: sprite.size.height))
        let location = CGPoint(x:CGRectGetWidth(self.frame) + 60, y: sprite.size.height / 2 )
        sprite.position = location
        sprite.zPosition = 100
        sprite.physicsBody.affectedByGravity = false
        sprite.physicsBody.mass = 200000
        sprite.physicsBody.allowsRotation = false;
        sprite.name = "bottom"
        sprite.physicsBody.categoryBitMask = ContactCategory.poleB
        sprite.physicsBody.collisionBitMask = ContactCategory.poleB | ContactCategory.player
        sprite.physicsBody.contactTestBitMask = ContactCategory.poleB | ContactCategory.player
        poles.append(sprite)
        return sprite
    }
    
    func setUpMidPole()->SKSpriteNode{
        let sprite = SKSpriteNode()
        sprite.setScale(1)
        sprite.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: 5, height: 270))
        let location = CGPoint(x:CGRectGetWidth(self.frame) + 105 , y: bottomPole.size.height + (CGFloat)(270 / 2))
        sprite.position = location
        sprite.zPosition = 100
        sprite.physicsBody.affectedByGravity = false
        sprite.physicsBody.mass = 200000
        sprite.physicsBody.allowsRotation = false;
        sprite.name = "mid"
        sprite.physicsBody.categoryBitMask = ContactCategory.poleMid
        sprite.physicsBody.contactTestBitMask = ContactCategory.poleMid | ContactCategory.player
        poles.append(sprite)
        return sprite
    }
    
    func gameOver()->(){
        let scene = GameOverScene(size: self.scene.size)
        scene.scaleMode = SKSceneScaleMode.AspectFill
    
        let transition = SKTransition.revealWithDirection(SKTransitionDirection.Down, duration: 1.0)
        self.scene.view.presentScene(scene, transition: transition)
        scene.addChild(myLabel)
    }
    

    
    
    
}
